# Minecraft Craft Helper

Recursively resolves provided crafting recipes to totals of components and provides detailed crafting instructions

## Installation

Simply download the [script](./craft-helper.py) via the download button in the top right of the code view or download this repository as a zip file and unzip the `python/minecraft-craft-helper` directory.

Minecraft Craft Helper uses the following packages which you may need to install:

```shell
pip install schema argparse pyyaml queue
```

## Usage

```
$ python craft-helper.py [-h] --recipes <file> [-i] items [items ...]
```

## Options

- **-r, --recipes**: Path to your recipes file (see below)
- **-i, --inventory** (optional): This flag enables showing the internal leftovers inventory used to keep track of items that output more than one item per crafting operation
- **-v, --verbose** (optional): This flag enables showing crafting components for each crafting step. This can help if your item has multiple recipes and you are not sure which recipe you had in your recipes file

## Recipes File

Place all recipes that should be resolved to their components into a yaml file. An [example file](./recipes-example.yml) and [schema](./recipes-schema.json) have been provided with the script. The file should contain a list of item objects containing the following properties:

- a `name`,
- an (optional) `outputs` amount (read: amount of the item created by the crafting recipe, default: 1)
- an (optional) `disabled` property (read: put `disabled: true` to disable the recipe)
- an (optional) `stackSize` (default: 64)
- a (required) list of `ingredients`, where each ingredient object must also have:
  - a `name`
  - an (optionally) `amount` (default: 1)

```yml
- name: Redstone Repeater
  # outputs is assumed to be 1
  ingredients:
    - name: Redstone Torch
      amount: 2
    - name: Redstone
      # amount is assumed to be 1
    - name: Stone
      amount: 3
- name: Wood Plank
  disabled: true # recipe is disabled
  outputs: 4
  ingredients:
    - name: Wood Logs
- name: Stick
  outputs: 4
  ingredients:
    - name: Wood Plank
      amount: 4
- name: Sign
  outputs: 6
  stackSize: 16 # stacks up to 16
  ingredients:
    - name: Wood Plank
      amount: 6
    - name: Stick
```

**Tip:** You can give your recipes file any name to organize for different mods or modpacks or organize them into folders.

## Example

```
$ python craft-helper.py -r recipes-example.yml "Redstone Repeater,2" "Redstone Block" "Sign *2"
```

**Output:**

```
Crafting List:
  - Redstone Repeater: 2
  - Redstone Block: 1
  - Sign: 2

Total Ingredients:
  - Redstone: 15
  - Wood Plank: 10
  - Cobblestone: 6

Steps:
  - Craft 1 of Redstone Block
  - Craft 6 of Stone
  - Craft 8 of Stick
  - Craft 6 of Sign
  - Craft 4 of Redstone Torch
  - Craft 2 of Redstone Repeater
```

## FAQ

<details>
<summary><b>Q:</b> Why does the example craft 6 Signs, when the input says 2?</summary>
<p><b>A:</b> Craft Helper tries to tell you the amount you will craft and since the crafting recipe for <i>Sign</i> outputs 6 items, it rounds up the 2 <i>Signs</i> to 36. For items crafted as ingredients for other crafting recipes Craft Helper will keep an internal inventory to make sure any extras made for one item are used for another (for example Sticks in the example are used for the 1 craft of <i>Signs</i> and 4 crafts of <i>Redstone Torch</i>)</p>
</details>

<details>
<summary><b>Q:</b> What are stack sizes used for?</summary>
<p><b>A:</b> Stack sizes are used for the crafting steps to show you how many stacks of an item (+ how many extra) you are making. For Example crafting 68 Sticks would be displayed as:<br/>- Craft 68 of Stick (1 stack + 4)</p>
</details>

## License

This script is published under the WTFPL license (see http://www.wtfpl.net/)
